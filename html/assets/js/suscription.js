function setLoading($selector, $text) {
  $($selector).block({
    message: $text,
    centerY: 0,
    css: {
      border: "none",
      padding: "15px",
      backgroundColor: "#9a7aa0",
      "-webkit-border-radius": "10px",
      "-moz-border-radius": "10px",
      opacity: 1,
      color: "#fff"
    },
    overlayCSS: { backgroundColor: "#78ded4" }
  });
}

function processPay() {
  var location = String(window.location);
  var form = document.getElementById("pay-form");
  var generalData = $("#general-form").serializeArray();
  var payData = $("#payment-form").serializeArray();

  var mergedData = $.merge(generalData, payData);

  $.ajax({
    type: "POST",
    url: location,
    data: $.param(mergedData),
    beforeSend: function() {
      setLoading("body", "Espere un momento, procesando transacción");
    },
    success: function(data) {
      $.unblockUI();

      $("form")
        .find("button")
        .prop("disabled", false);

      if (data.error) {
        $("html, body").animate({ scrollTop: 0 }, "slow");

        //Open pay transaction error handling
        if (data.error.open_pay_code) {
          $(".pay-errors")
            .html("<p>" + getSpanishError(data.error.open_pay_code) + "</p>")
            .removeClass("d-none");
        }

        //Open pay message error handling
        if (data.error.open_pay_message) {
          $(".pay-errors")
            .html("<p>" + data.error.open_pay_message + "</p>")
            .removeClass("d-none");
        }

        //Database error handling
        if (data.error.errors) {
          var errorMessages = "";
          $.each(data.error.errors, function(key, val) {
            errorMessages += "<p class='mb-1'>" + val + "</p>";
          });
          $(".pay-errors")
            .html(errorMessages)
            .removeClass("d-none");
        }
      } else {
      

        $(".pay-errors").addClass("d-none");
        $(".pay-success").removeClass("d-none");

        $("#tbName,#tbEmail,#tbAddress,#tbState,#tbZip,#tbCountry").text("");

        document.getElementById("payment-form").reset();

      }
    },
    error: function(error) {
      $.unblockUI();
      $("html, body").animate({ scrollTop: 0 }, "slow");

      $("form")
        .find("button")
        .prop("disabled", false);
    }
  });
}

//jQuery generate the token on submit.
$(function() {
  OpenPay.setId("mxqd0md6sxov3pmg4xew");
  OpenPay.setApiKey("pk_aed17224750f45a5adbf03bc95961e36");
  OpenPay.setSandboxMode(true);
  //Se genera el id de dispositivo
  var deviceSessionId = OpenPay.deviceData.setup(
    "payment-form",
    "deviceIdHiddenFieldName"
  );

  var sucess_callback = function(response) {
    var token_id = response.data.id;
    $("#token_id").val(token_id);
    processPay();
  };

  var error_callback = function(response) {
    if (response.data.error_code) {
      var description = getSpanishError(response.data.error_code);
      $(".pay-errors")
        .html("<p>" + description + "</p>")
        .removeClass("d-none");
    }
    $("#pay-button").prop("disabled", false);
  };

  $("#general-form").submit(function(e) {
    $("#suscription-container").addClass("fadeOutLeft animated fast");

    setTimeout(function() {
      $("#suscription-container").addClass("d-none");
      $("#payment-container")
        .removeClass("d-none")
        .removeClass("fadeOutRight")
        .addClass("fadeInRight animated fast");
      $("html, body").animate({ scrollTop: 0 }, "slow");
    }, 500);

    $("#tbName").text($("input[name='client[name]']").val());
    $("#tbEmail").text($("input[name='client[email]']").val());
    $("#tbAddress").text($("input[name='client[address]']").val());
    $("#tbState").text($("input[name='client[state]']").val());
    $("#tbZip").text($("input[name='client[zip]']").val());
    $("#tbCountry").text($("input[name='client[country]']").val());

    return false;
  });

  $("#editData").on("click", function() {
    $("#payment-container").addClass("fadeOutRight animated fast");

    $("#suscription-container").removeClass("fadeOutLeft");

    setTimeout(function() {
      $("#payment-container").addClass("d-none");
      $("#suscription-container")
        .removeClass("d-none")
        .addClass("fadeInLeft animated fast");
      $("html, body").animate({ scrollTop: 0 }, "slow");
    }, 500);

    return false;
  });

  $("#payment-form").submit(function(event) {
    $("#pay-button").prop("disabled", true);
    $(".pay-errors").addClass("d-none");

    OpenPay.token.extractFormAndCreate(
      "payment-form",
      sucess_callback,
      error_callback
    );
    return false;
  });
});
