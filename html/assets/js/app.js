// Header Spacing
var myHeader = document.querySelector("header");
$(".header-spacing").css("margin-top", $(myHeader).height());
var headroom = new Headroom(myHeader, {
  offset: $(myHeader).height(),
  tolerance : {
        up : 30,
        down : 0
	}
});
headroom.init(); 

$( window ).on( "load", function() {
    $('.loader svg').fadeOut( "slow" );
    $('.loader').delay(700).fadeOut( "slow" );
});

$('#modal-toggle').click(function(e) {
    e.preventDefault();
  	$('.embed-responsive').html('<iframe width="1200" height="675" src="https://www.youtube.com/embed/cWPFwJ-cv3Y?rel=0&autoplay=1" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>');
	$('#myModal').modal('show')
});

$('#myModal').on('hidden.bs.modal', function () {
    $('.embed-responsive').html('');
});

$(function() {
    // Desired offset, in pixels
    var offset = $(myHeader).height();
    // Desired time to scroll, in milliseconds
    var scrollTime = 800;


    $('a[href^="#"]').click(function() {
        if ( $(this).hasClass("down-btn")) {
            // Need both `html` and `body` for full browser support
            $("html, body").animate({
                scrollTop: $( $(this).attr("href") ).offset().top - offset 
            }, scrollTime);

            // Prevent the jump/flash
            return false;
        }
    });
});

// function openChat(status){
//     if( status === 'online' ){
//         var msg = 'Hola, ¿cómo podemos ayudarle?';
//         jivo_api.showProactiveInvitation( msg ); 
//     } else {
//         jivo_api.open();
//     }
// }x

var invitationSent = false;

$('[data-chat="open"]').click(function(){
    var status = jivo_api.chatMode();
    if( status === 'online' && !invitationSent ){
        if ( $(this).attr('data-msg') ) {
            var msg = $(this).data("msg")
            jivo_api.showProactiveInvitation(msg);
            console.log("1");
        }
        else {
            var msg = 'Hola, ¿cómo podemos ayudarle?';
            jivo_api.showProactiveInvitation(msg);
            console.log("2");
        }
    invitationSent = true;
    } else {
        jivo_api.open();
        console.log("3");
    }
    ga('send', {'hitType': 'pageview', 'page': '/cta-click', 'title': 'Click en CTA'});
});

/* -----------------
      CONTACT FORM
------------------ */

    function revealMessage(msg,form){
        form.find('.form-message').html(msg).addClass('open');
        setTimeout(function() {
            form.find('.form-message').html('').removeClass('open');
        }, 2000);
    }

    $('.contact-form').submit(function(e) {
        // Prevent the form from actually submitting
        e.preventDefault();
        var form = $(this);

        // Get the post data
        var data = $(this).serialize();

        // block multiple clicks
        form.find('input[submit]').attr('disabled',true);

        // Send it to the server
        $.post('/', data, function(response) {
            console.log(response);
            if (response.success) {
                revealMessage('Gracias por registrarte, te contactaremos cuanto antes.',form);
            } else {
                revealMessage('Hubo un error, intenta de nuevo más tarde.',form);
            }
            form.find('input[submit]').attr('disabled',false);
        });
    });
