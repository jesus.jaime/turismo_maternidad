var map = L.map('map').setView([37.8, -96], 4);
var doctors = [];

function renderMap() {
    const mapboxAccessToken = 'pk.eyJ1IjoiZG9vZHNvZnR3YXJlIiwiYSI6ImNqb2VoYm9xazJ3YTgzcHB1YjFxbzV5aWMifQ.RfJd-HLMnJagBxN51AFXZg';

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + mapboxAccessToken, {
        id: 'mapbox.light',
    }).addTo(map);
}

function setUSALayout() {
    var geojson;

    geojson = L.geoJson(statesData, {
        style: style,
        onEachFeature: onEachFeature
    }).addTo(map);

    function style() {
        return {
            fillColor: '#B40079',
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7
        };
    }

    //Set Usa State Border on mouseover
    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#F3A0DC',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }
    }

    //Remove border on mouseleft
    function resetHighlight(e) {
        geojson.resetStyle(e.target);
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight
        });
    }




}

function renderMarkers() {

    var location = $("#DATA").data('url') + 'hospitals/fetchAll';
    const icon = L.icon({
        iconUrl: $("#DATA").data('url') + 'assets/img/miscellaneous/map-icon.png',
        iconSize: [35, 35], // size of the icon
    });

    $.ajax({
        'type': 'GET',
        'url': location,
        success: function (data) {
            $.each(data, function (key, val) {
                const marker = L.marker([val.latitude, val.longitude], { icon: icon }).addTo(map);
                marker.bindPopup("<b>" + val.name + "</b><br>" + val.address).openPopup();
                marker.hospitalSlug = val.slug;
                marker.on('click', function (data) {
                    getDoctorsByFilter(data.target.hospitalSlug);
                });

            });



        }, error: function () {

        }
    });

}





function getDoctorsByFilter(hospitalSlug) {
    var location = $("#DATA").data('url') + 'filterDoctors';
    $.ajax({
        type: "POST",
        url: location,
        data: {
            'hospital'          :   hospitalSlug,
            'state'             :   $("#stateFilter").val(),
            'specialism'        :   $("#specialismFilter").val(),
            'service'           :   $("#serviceFilter").val(),
            'CRAFT_CSRF_TOKEN'  :   $("input[name='CRAFT_CSRF_TOKEN']").val()

        },
        beforeSend: function () {
            $('.ajax-loader').removeClass('d-none');
        },
        success: function (data) {
            doctors = data;
            $(".sw-btn-next").click();
            $('.ajax-loader').addClass('d-none');
        },
        error: function (error) {
            $('.ajax-loader').addClass('d-none');
        }
    })
}



$(function () {


    $("select").select2({
        language: 'es'
    });

    $('#applyFilter').on('click', function (e) {
        getDoctorsByFilter();
    });

    var renderMapPromise = new Promise(function (resolve, reject) {
        try {
            renderMap();
            setUSALayout();
            renderMarkers();
            resolve(true);
        } catch (Exception) {

            reject('Error on render map');
        }
    });

    renderMapPromise.then(function () {



        // Step show event
        $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
            //alert("You are on step "+stepNumber+" now");
            if (stepPosition === 'first') {
                $("#prev-btn").addClass('disabled');
            } else if (stepPosition === 'final') {
                $("#next-btn").addClass('disabled');
            } else {
                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');
            }
        });

        // Toolbar extra buttons
        var btnFinish = $('<button></button>').text('Finish')
            .addClass('btn btn-info')
            .on('click', function () { alert('Finish Clicked'); });
        var btnCancel = $('<button></button>').text('Cancel')
            .addClass('btn btn-danger')
            .on('click', function () { $('#smartwizard').smartWizard("reset"); });


        // Smart Wizard
        $('#smartwizard').smartWizard({
            selected: 0,
            theme: 'default',
            transitionEffect: 'fade',
            showStepURLhash: true,
            toolbarSettings: {
                toolbarPosition: 'bottom',
                toolbarButtonPosition: 'end',
                toolbarExtraButtons: [btnFinish, btnCancel]
            }
        });


        // External Button Events
        $("#reset-btn").on("click", function () {
            // Reset wizard
            $('#smartwizard').smartWizard("reset");
            return true;
        });

        $("#prev-btn").on("click", function () {
            // Navigate previous
            $('#smartwizard').smartWizard("prev");
            return true;
        });

        $("#next-btn").on("click", function () {
            // Navigate next
            $('#smartwizard').smartWizard("next");
            return true;
        });

        $("#theme_selector").on("change", function () {
            // Change theme
            $('#smartwizard').smartWizard("theme", $(this).val());
            return true;
        });

        // Set selected theme on page refresh
        $("#theme_selector").change();





    }).catch(function () {

    });













});