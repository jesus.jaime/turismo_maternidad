<?php

namespace Craft;

class NutisPaysPlugin extends BasePlugin
{



    public function init()
    {
        require_once __DIR__ . '/vendor/autoload.php';
    }

    public function getName()
    {

        return Craft::t('Nutis Pays');
    }

    public function getVersion()
    {
        return '1.0.0';
    }

    public function getDeveloper()
    {
        return 'José de Jesús Jaime';
    }

    public function getDeveloperUrl()
    {
        return '';
    }

    public function hasCpSection()
    {
        return true;
    }

    /**
     * Register control panel routes
     */
    public function hookRegisterCpRoutes()
    {
        return array(
            'nutispays\/clientDetails\/(?P<clientId>\d+)' => 'nutispays/clients/_pays',
        );
    }

    /**
     * @return array
     */
    protected function defineSettings()
    {
        return array(
            'ID' => array(AttributeType::String, 'required' => true),
            'publicKey' => array(AttributeType::String, 'required' => true),
            'privateKey' => array(AttributeType::String, 'required' => true),
        );
    }

    public function getSettingsHtml()
    {
        return craft()->templates->render('nutispays/_settings', array(
            'settings' => $this->getSettings()
        ));
    }


    public function registerSiteRoutes()
    {

        return array(
            'nutisPays/processPay' => array(
                'action' => 'nutis/processPay'
            )
        );
    }


}