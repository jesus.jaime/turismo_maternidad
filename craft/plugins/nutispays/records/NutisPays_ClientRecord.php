<?php
namespace Craft;

class NutisPays_ClientRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'nutispays_client';
    }

    protected function defineAttributes()
    {

        return array(
            'pay_id' => array(AttributeType::String, 'required' => true, 'unique' => true),
            'name' => array(AttributeType::String, 'required' => true, 'unique' => false),
            'email' => array(AttributeType::Email, 'required' => true, 'unique' => false),
            'address    ' => array(AttributeType::String, 'required' => true, 'unique' => false),
            'state' => array(AttributeType::String, 'required' => true, 'unique' => false),
            'zip' => array(AttributeType::String, 'required' => true, 'unique' => false),
            'country' => array(AttributeType::String, 'required' => true, 'unique' => false),
            'periodEndDate' => array(AttributeType::String, 'required' => true, 'unique' => false)
        );
    }

    public function defineRelations()
    {
        return array(
            'pays' => array(static::HAS_MANY, 'NutisPays_PayRecord', 'clientId'),
        );
    }



    /**
     * Create a new instance of the current class. This allows us to
     * properly unit test our service layer.
     *
     * @return BaseRecord
     */
    public function create()
    {
        $class = get_class($this);
        $record = new $class();

        return $record;
    }
}
