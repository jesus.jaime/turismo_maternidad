<?php
namespace Craft;

class NutisPays_PayRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'nutispays_client_pays';
    }

    protected function defineAttributes()
    {
        return array(
            'clientId' => array(AttributeType::Number, 'required' => true),
            'order_id' => array(AttributeType::String, 'required' => true, 'unique' => true),
            'product' => array(AttributeType::String, 'required' => true),
            'quantity' => array(AttributeType::Number, 'required' => true),
            'unit_price' => array(AttributeType::Number, 'required' => true),
            'amount' => array(AttributeType::Number, 'required' => true),
            'currency' => array(AttributeType::Enum, 'values' => "USD,MXN", 'required' => true),
            'payment_method' => array(AttributeType::String, 'required' => true),
            'status' => array(AttributeType::String, 'required' => true)
        );
    }

    public function defineRelations()
    {
        return array(
            'client' => array(static::BELONGS_TO, 'NutisPays_ClientRecord', 'clientId'),
        );
    }

    /**
     * Create a new instance of the current class. This allows us to
     * properly unit test our service layer.
     *
     * @return BaseRecord
     */
    public function create()
    {
        $class = get_class($this);
        $record = new $class();

        return $record;
    }
}
