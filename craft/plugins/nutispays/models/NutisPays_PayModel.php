<?php
namespace Craft;

class NutisPays_PayModel extends BaseModel
{
    /**
     * Defines what is returned when someone puts {{ pay }} directly
     * in their template.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
    protected function defineAttributes()
    {
        return array(
            'id' => AttributeType::Number,
            'clientId' => AttributeType::Number,
            'order_id' => AttributeType::String,
            'product' => AttributeType::String,
            'quantity' => AttributeType::Number,
            'unit_price' => AttributeType::Number,
            'amount' => AttributeType::Number,
            'currency' => array(AttributeType::Enum, 'values' => "usd,mxn"),
            'payment_method' => AttributeType::String,
            'dateCreated' => AttributeType::DateTime,
            'status' => AttributeType::String
        );
    }

}
