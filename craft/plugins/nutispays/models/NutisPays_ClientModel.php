<?php
namespace Craft;

/**
 * Client Model
 *
 * Provides a read-only object representing an ingredient, which is returned
 * by our service class and can be used in our templates and controllers.
 */
class NutisPays_ClientModel extends BaseModel
{


    /**
     * Defines what is returned when someone puts {{ client }} directly
     * in their template.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Define the attributes this model will have.
     *
     * @return array
     */
    public function defineAttributes()
    {
        return array(
            'id'=>AttributeType::Number,
            'pay_id'=>AttributeType::String,
            'name' => AttributeType::String,
            'email' => AttributeType::Email,
            'address' => AttributeType::String,
            'state' => AttributeType::String,
            'zip' => AttributeType::String,
            'country' => AttributeType::String,
            'periodEndDate' => AttributeType::String
        );
    }
}
