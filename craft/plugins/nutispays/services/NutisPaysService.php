<?php

namespace Craft;


/**
 * Nutis Pays Service
 *
 * Provides a consistent API for our plugin to access the database
 */
class NutisPaysService extends BaseApplicationComponent
{
    protected $clientRecord;
    protected $payRecord;

    /**
     * Create a new instance of the Nutis Pays Service.
     * Constructor allows ClientRecord dependency to be injected to assist with unit testing.
     *
     * @param @clientRecord ClientRecord The client record to access the database
     * @param @payRecord PayRecord The pay record to access the database
     */
    public function __construct($clientRecord = null, $payRecord = null)
    {
        $this->clientRecord = $clientRecord;
        $this->payRecord = $payRecord;
        if (is_null($this->clientRecord)) {
            $this->clientRecord = NutisPays_ClientRecord::model();
        }
        if (is_null($this->payRecord)) {
            $this->payRecord = NutisPays_PayRecord::model();
        }


        $settings = craft()->plugins->getPlugin('nutispays')->getSettings();



    }

    public function getCriteria($table, array $attributes = array())
    {
        return craft()->elements->getCriteria($table, $attributes);
    }


    /**
     * Get a new blank client
     *
     * @param  array                           $attributes
     * @return NutisPays_ClientModel
     */
    public function newClient($attributes = array())
    {
        $model = new NutisPays_ClientModel();
        $model->setAttributes($attributes);

        return $model;
    }



    /**
     * Save a new  client back to the database.
     *
     * @param  NutisPays_ClientModel $model
     * @return array
     */
    public function saveClient(NutisPays_ClientModel &$model)
    {

        $record = $this->clientRecord->create();
        $record->setAttributes($model->getAttributes());

        if ($record->save()) {
            return array("status" => 'saved', "id" => $record->getAttribute('id'));
        } else {
            $model->addErrors($record->getErrors());

            return array("status" => 'error');
        }
    }


    /**
     * Get a new blank pay
     *
     * @param  array                           $attributes
     * @return NutisPays_PayModel
     */
    public function newPay($attributes = array())
    {
        $model = new NutisPays_PayModel();
        $model->setAttributes($attributes);

        return $model;
    }


    /**
     * Save a new  client pay back to the database.
     *
     * @param  NutisPays_ClientPay $model
     * @return array
     */

    public function savePay(NutisPays_PayModel &$model)
    {

        $record = $this->payRecord->create();
        $record->setAttributes($model->getAttributes());


        if ($record->save()) {
            return true;
        } else {
            $model->addErrors($record->getErrors());

            return false;
        }


    }

    /**
     * Get all clients from the database.
     *
     * @return array
     */
    public function getAllClients()
    {

        $records = $this->clientRecord
            ->with('pays')
            ->findAll(array(
                'order' => 't.dateCreated DESC'
            ));

    
        return $records;
    }

    /**
     * Get client's pays from the database based on ID. If no pay exists, null is returned.
     *
     * @param  int   $id
     * @return mixed
     */
    public function getClientPays($id)
    {
        $records = $this->payRecord->findAll(array(
            'condition' => 'clientId=:clientID',
            'params' => array(
                ':clientID' => $id
            )
        ));


        $newRecords = NutisPays_PayModel::populateModels($records, 'id');



        return $newRecords;
    }


    /**
     * Get all pays from the database.
     *
     * @return array
     */
    public function getAllPays()
    {
        $records = $this->payRecord->findAll(array('order' => 't.dateCreated DESC'));

        return NutisPays_PayModel::populateModels($records, 'id');
    }


    /**
     * Get a specific pay from the database based on ID. If no pay exists, null is returned.
     *
     * @param  int   $id
     * @return mixed
     */
    public function getPayById($id)
    {
        if ($record = $this->payRecord->findByPk($id)) {

            return [
                " client " => $record->client,
                " pay " => NutisPays_PayModel::populateModel($record)
            ];
        }
    }







}
