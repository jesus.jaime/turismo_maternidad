<?php
namespace Craft;




class NutisPaysController extends BaseController
{
    protected $allowAnonymous = true;


    public function actionProcessPay()
    {
        $settings = craft()->plugins->getPlugin('nutispays')->getSettings();

        $this->requireAjaxRequest();



        try {


            $openpay = \Openpay::getInstance(
                $settings["ID"],
                $settings["privateKey"]
            );

            //Create customer
            $customerData = array(
                'name' => craft()->request->getPost('client')["name"],
                'last_name' => '',
                'email' => craft()->request->getPost('client')["email"]
            );



            $customer = $openpay->customers->add($customerData);


            //Save customer's card
            $cardDataRequest = array(
                'holder_name' => craft()->request->getPost('card')["holdername"],
                'card_number' => craft()->request->getPost('card')["number"],
                'cvv2' => craft()->request->getPost('card')["cvv2"],
                'expiration_month' => craft()->request->getPost('card')["expiration_month"],
                'expiration_year' => craft()->request->getPost('card')["expiration_year"]
            );
            $card = $customer->cards->add($cardDataRequest);


            //Create first charge
            $chargeData = array(
                'method' => 'card',
                'source_id' => craft()->request->getPost('token_id'),
                'amount' => (float)craft()->request->getPost('order')["amount"],
                'description' => craft()->request->getPost('order')["product"],
                'device_session_id' => craft()->request->getPost('deviceIdHiddenFieldName'),
                'customer' => $customerData
            );

            $charge = $openpay->charges->create($chargeData);


            //Create customer's subscription
            $subscriptionDataRequest = array(
                "trial_end_date" => date('Y-m-d'),
                'plan_id' => 'pcwurpj4yfh7g5anzgto', //Cambiar por plan de produccion
                'card_id' => $card->id
            );

            $subscription = $customer->subscriptions->add($subscriptionDataRequest);


            $transaction = craft()->db->beginTransaction();

            try {

                //Save client on database
                $cattributes = craft()->request->getPost('client');
                $cattributes["pay_id"] = $customer->id;
                $cattributes["periodEndDate"] = $subscription->period_end_date;
                $cmodel = craft()->nutisPays->newClient($cattributes);

                $cRecordResult = craft()->nutisPays->saveClient($cmodel);

                switch ($cRecordResult["status"]) {
                    case 'saved':
                    case 'registered':


                        //Save client new pay
                        $attributes = [
                            "clientId" => $cRecordResult["id"],
                            "order_id" => $charge->id,
                            "product" => craft()->request->getPost('order')["product"],
                            "quantity" => craft()->request->getPost('order')["quantity"],
                            "unit_price" => craft()->request->getPost('order')["unit_price"] / 100,
                            "amount" => craft()->request->getPost('order')["amount"] / 100,
                            "currency" => craft()->request->getPost('order')["currency"],
                            "payment_method" => $charge->card->brand,
                            "status" => $charge->status
                        ];
                        $pmodel = craft()->nutisPays->newPay($attributes);
                        $pRecordResult = craft()->nutisPays->savePay($pmodel);
                        if (!$pRecordResult) {
                            $this->returnErrorJson(array("message" => "Error al guardar el pago", "errors" => $pmodel->getErrors()));
                        }


                        $transaction->commit();



                        $this->returnJson(array("message" => "Pago registrado correctamente"));

                        break;

                    default:
                        $transaction->rollback();
                        $this->returnErrorJson(array("message" => "Error al guardar el pago", "errors" => $cmodel->getErrors()));

                }


            } catch (Exception $e) {
                $transaction->rollback();

                $this->returnErrorJson(array("message" => "Error al guardar el pago"));


            }

        } catch (\OpenpayApiTransactionError $e) {
            $this->returnErrorJson(array("open_pay_code" => $e->getErrorCode()));

        } catch (\OpenpayApiRequestError $e) {
            $this->returnErrorJson(array("open_pay_message" => $e->getMessage()));

        } catch (\OpenpayApiConnectionError $e) {
            $this->returnErrorJson(array("open_pay_message" => $e->getMessage()));

        } catch (\OpenpayApiAuthError $e) {
            $this->returnErrorJson(array("open_pay_message" => $e->getMessage()));

        } catch (\OpenpayApiError $e) {
            $this->returnErrorJson(array("open_pay_message" => $e->getMessage()));

        } catch (\Exception $e) {
            $this->returnErrorJson(array("open_pay_message" => $e->getMessage()));
        }

    }
}