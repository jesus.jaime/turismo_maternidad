<?php

namespace Craft;

/**
 * NutisPays Variable provides access to database objects from templates
 */
class NutisPaysVariable
{
    /**
     * Get all available ingredients
     *
     * @return array
     */
    public function getAllPays()
    {
        return craft()->nutisPays->getAllPays();
    }


    public function getPayById($id)
    {
        return craft()->nutisPays->getPayById($id);
    }

    public function getAllClients()
    {
        return craft()->nutisPays->getAllClients();
    }
    
    public function getClientPays($id){
        return craft()->nutisPays->getClientPays($id);

    }
}
